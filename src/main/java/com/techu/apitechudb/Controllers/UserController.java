package com.techu.apitechudb.Controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "ageFilter", required = false, defaultValue = "0") int ageFilter
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de ageFilter es " +ageFilter);

        return new ResponseEntity<>(this.userService.findAll(ageFilter), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("La descripción del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateProduct(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va a actualizar en parametro es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId());
        System.out.println("El nombre del usuario que se va a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deleteProduct = this.userService.delete(id);

        return new ResponseEntity<> (
                deleteProduct ? "Usuario borrado" : "Usuario no encontrado" ,
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/users/{age}")
    public ResponseEntity<Object> getUserByAge(@PathVariable int age) {
        System.out.println("getUserByAge");
        System.out.println("La edad del usuario a buscar es " + age);

        Optional<UserModel> result = this.userService.findByAge(age);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Edad no encontrada",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
