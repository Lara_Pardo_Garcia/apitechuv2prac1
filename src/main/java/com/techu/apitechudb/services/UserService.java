package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(int ageFilter) {
        System.out.println("findAll in UserService");

        if (ageFilter <= 0) {
            return this.userRepository.findAll();
        }

        System.out.println("ageFilter es " + ageFilter);

        ArrayList<UserModel> result = new ArrayList<>();

        for (UserModel userInList : ApitechudbApplication.userModels) {
            if (userInList.getAge() == ageFilter) {
                System.out.println("Usuario " + userInList.getName() + "tiene misma edad que filtro");
                result.add(userInList);
            }
        }
        return result;
    }

    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel){
        System.out.println("update en UserService");

        return this.userRepository.update(userModel);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");

        boolean result = false;

        Optional<UserModel> productToDelete = this.findById(id);

        if (productToDelete.isPresent() == true) {
            result = true;
            this.userRepository.delete(productToDelete.get());
        }
        return result;
    }

    public Optional<UserModel> findByAge(int age) {
        System.out.println("findByAge en UserService");

        return this.userRepository.findByAge(age);
    }

}
